cd
PWD=`pwd`

#查找所有带.xcodeproj后缀的文件
files=`find ${WORKSPACE} -name "*.xcodeproj"`

for path in "$files"
do
	#获取带后缀的文件名
	fileName=`basename $path`
	pathlen=${#fileName}
	ext="*.xcodeproj"
	suffixlen=${#ext}

	if [ $pathlen -ge $suffixlen ]
	then
		#获取项目名称
		projName=${fileName:0:$pathlen-$suffixlen+1}
		plistPath=${WORKSPACE}/${projName}/Info.plist

		#判断文件是否存在
		if [ ! -e "$plistPath" ]
		then 	
			plistPath=${WORKSPACE}/${projName}/${projName}-Info.plist
		fi
	fi
	break;
done

#上传IPA到 140
plistBuddyPath=/usr/libexec/PlistBuddy
uploadDate=`date +%Y%m%d_%H%M`
appVersion=`${plistBuddyPath} -c "Print CFBundleShortVersionString" ${plistPath}`
appBuild=`${plistBuddyPath} -c "Print CFBundleVersion" ${plistPath}`

resourceIpaPath=${WORKSPACE}/build/${BUILD_NUMBER}.ipa
uploadIpaName=${JOB_NAME}_${BUILD_NUMBER}_${uploadDate}_v${appVersion}\(${appBuild}\).ipa

resourceDSYMPath=${WORKSPACE}/build/${BUILD_NUMBER}-dSYM.zip
uploadDSYMName=${JOB_NAME}_${BUILD_NUMBER}_${uploadDate}_v${appVersion}\(${appBuild}\)-dSYM.zip

HOST='192.168.16.140'

USER='ci'
PASSWD='111111'
DIR=`date +%Y%m%d`

ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
binary
mkdir $DIR
put $resourceIpaPath ${DIR}/${uploadIpaName}
put $resourceDSYMPath ${DIR}/${uploadDSYMName}
quit
END_SCRIPT
