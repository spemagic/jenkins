# !bin/bash

#READ_ME
#不同项目的 releaseID 不一样， 换项目之后需要替换 releaseID （releaseID就是 project.pbxproj 文件中 Release 前面的那一串字符串，每个target唯一且不会变）
#targetProvisioningFile 换成自己需要的描述文件符

#替换configFilePath

#获取用户目录
cd
PWD=`pwd`

DEBUG=0

if [ $DEBUG == 1 ]
then
	WORKSPACE=/Users/apple/.jenkins/jobs/imoffice/workspace
fi

#查找所有带.xcodeproj后缀的文件
files=`find ${WORKSPACE} -name "*.xcodeproj"`

for path in "$files"
do
	echo "Find project path [$path]"

	#获取带后缀的文件名
	fileName=`basename $path`
	pathlen=${#fileName}
	ext="*.xcodeproj"
	suffixlen=${#ext}

	if [ $pathlen -ge $suffixlen ]
	then
		#获取项目名称
		projName=${fileName:0:$pathlen-$suffixlen+1}
		plistPath=${WORKSPACE}/${projName}/Info.plist

		#判断文件是否存在
		if [ ! -e "$plistPath" ]
		then 	
			plistPath=${WORKSPACE}/${projName}/${projName}-Info.plist
		fi

		plistBuddyPath=/usr/libexec/PlistBuddy

		#修改 imo 班聊 App运行环境
		switchFilePath=${WORKSPACE}/${projName}/IMOSwitch.h

		environmentFilePath=${PWD}/jenkins/config/SwitchAppEnvironment.plist
		echo $environmentFilePath

		isOnline=`${plistBuddyPath} -c "Print onlineMode" ${environmentFilePath}`
		mv $switchFilePath ${switchFilePath}.tmp

		if [ $isOnline == false ] ;then
			echo "offline"
			sed 's/.*IMO_CONNECT_OFFICIAL_SERVER.*/\/\/#define IMO_CONNECT_OFFICIAL_SERVER/' ${switchFilePath}.tmp > ${switchFilePath}
		else
			echo "online"
			sed 's/.*IMO_CONNECT_OFFICIAL_SERVER.*/#define IMO_CONNECT_OFFICIAL_SERVER/' ${switchFilePath}.tmp > ${switchFilePath}
		fi
		rm ${switchFilePath}.tmp

		echo "Find plist path [$plistPath]"

		#修改build版本号

		echo "Read currentBuild for ${plistPath}"

        currentBuild=`${plistBuddyPath} -c "Print CFBundleVersion" ${plistPath}`

        echo "currentBuild from plistBuddy [$currentBuild]"

        if [ $DEBUG == 1 ]
		then
			appBuild=`expr ${currentBuild} + 1`
		else
			appBuild=`expr ${currentBuild} + ${BUILD_NUMBER}`
		fi
        
        echo "Change appBuild to [$appBuild]"

		${plistBuddyPath} -c "Set :CFBundleVersion $appBuild" "${plistPath}"
		echo "Change build version to ${appBuild}"

		#获取配置文件路径
		configFilePath=${WORKSPACE}/${projName}.xcodeproj/project.pbxproj
		echo "ConfigFilePath ${configFilePath}"

		#获取Release版本的标识符
		releaseID="CA5541A81518530D00C19406"
		orgin=$(grep -i -n "^.*${releaseID}.*=" $configFilePath | head -n 1 | awk -F ':' '{print $1}')
		count=$(grep -i -A 200 "^.*${releaseID}.*=" $configFilePath | grep -i -n 'PROVISIONING_PROFILE' | head -n 1 |awk -F ':' '{print $1}')

		#Provisioning profile描述符所在行
		let line=$orgin+count-1
		echo "Find releaseID in line [$line]"
		#替换成指定的描述符 imo_push_adhoc
		targetProvisioningFile="b153b972-9f32-4c8e-8da9-565e12f10360"
		sed -i '' $line"s/^.*/                PROVISIONING_PROFILE = "${targetProvisioningFile}";/g" $configFilePath
		echo "Change Provisioning profile to [${targetProvisioningFile}]"
	fi

	break;
done

